$(document).ready( function() {

	var url = "resist.php?";

	$('input#target').focus( function() {
		$('#results.alert').html('');
		$('#results.alert').attr('class', "");
	});

	$('input#submit').on("click", function() {
		$.get( url + $('#resist_form').serialize() , [], function(data) {
			// If "FAIL" is contained in the result
			if ( data.indexOf("FAIL") > -1 ) {
				$('#results').attr('class', "");
				$('#results').addClass('alert alert-danger');
				$('#results').html('<b>No Resistors within error.</b>');
			}
			else {
				var result = JSON.parse(data);
				$('#results').attr('class', "");
				$('#results').addClass('panel panel-primary');
				$('#results').html("<b>Parallel 1% Resistor Pairs for " + $("#target").val() + " " + $("#unit option:selected").text() + "</b>");
				$('#results').append('<table class="table table-striped table-bordered table-condensed"><thead><tr><th></th><th>Actual Resistance</th><th>R1</th><th>R2</th></tr></thead></tbody');
				$(result).each( function(i, d) {
					$('#results table').append('<tr><td>' + (i + 1) + '</td><td>' + d.RT/(Math.pow(10,$("#unit option:selected").val())) + ' ' + $("#unit option:selected").text() + '</td><td>' + d.R1/(Math.pow(10,$("#unit option:selected").val())) + ' ' + $("#unit option:selected").text() + '</td><td>' + d.R2/(Math.pow(10,$("#unit option:selected").val())) + ' ' + $("#unit option:selected").text() + '</td></tr>')
				});
				$('#results').append('</tbody></table>'); 			
			}

		});
	});

});
