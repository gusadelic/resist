<?php

include('standard.php');

$rT = $_GET['target'];
$input_decade = $_GET['unit'];

if ( isset($_GET['debug'])) {
	$debug == true;
}
else { $debug = false; }


while ( $rT >= 100 ) {
	$rT = $rT/10;
	$input_decade++;
}



if ( $rT >= 50 ) {
 	$r1decade = $input_decade + 1;
 	$r2decade = $input_decade + 1;
 	$r1 = $rT * pow(10,$r1decade - 1 ) * 2;
 	$r2 = $rT * pow(10,$r2decade - 1 ) * 2;
 }
 else {
	$r1decade = $input_decade;
	$r2decade = $input_decade;
	$r1 = $rT * pow(10,$r1decade) * 2;
	$r2 = $rT * pow(10,$r2decade) * 2;
}

if ( $debug ) echo("input_decade=" . $input_decade . "<br />");


$rT = $_GET['target'] * pow(10,$_GET['unit']);

$error_init = 0.001;
$error = $rT * $error_init;

$pairs = $_GET['pairs'];


$r1index = checkSTD($standard, $r1,  pow(10,$r1decade) );
$r2index = checkSTD($standard, $r2,  pow(10,$r2decade) );

if ( $debug ) echo ("Before Pairs: r1=" . $r1 . " r2=" . $r2 . " rT=" . $rT . " error=" . $error . " r1index=" . $r1index . " r2index=" . $r2index . "<br />");




$output = array();
$index = 0;




if ( $r1index < 0 ) {
	$r1index = $r1index * (-1);
}

if ( $r2index < 0 ) {
	$r2index = $r2index * (-1);
	$r2index--;
}

$r1 = $standard[$r1index] * pow(10,$r1decade);
$r2 = $standard[$r2index] * pow(10,$r2decade);

$rTprime = 1 / ( (1/$r1) + (1/$r2) );

$result = checkPair($rTprime, $rT, $error);


$loop = 0;
$last = 2;
if ( $debug ) echo ("  Before result loop: r1=" . $r1 . " r2=" . $r2 . " rT=" . $rT . " error=" . $error . " result=" . $result . "<br />");

while ( $pairs > 0 ) {

	while ( $result != 0 ) {
		
		if ( $result < 0 ) {
			if ( $standard[$r1index] * pow(10,$r1decade) > $standard[$r2index] * pow(10,$r2decade) ) {
				if ( $r1index == count($standard) - 1) {
					$r1decade++;
					$r1index = 0;
				}
				else {	
					$r1index++;
				}
			}
			else {
				if ( $r2index == count($standard) - 1) {
					$r2decade++;
					$r2index = 0;
				}
				else {
					$r2index++;
				}
			}
		}
		else if ( $result > 0 ) {
			if ( $standard[$r1index] * pow(10,$r1decade) < $standard[$r2index] * pow(10,$r2decade) ) {
				if ( $r1index == 0 && $r1decade > 0 ) {
					$r1decade--;
					$r1index = count($standard) - 1;
				}
				else if ( $r1index == 0 && $r1decade == 0 ) {
					if ( $r2index == count($standard) - 1) {
						$r2decade++;
						$r2index = 0;
					}
					else {
						$r2index++;
					}
				}
				else {
					$r1index--;
				}
			}
			else {
				if ( $r2index == 0 && $r2decade > 0 ) {
					$r2decade--;
					$r2index = count($standard) - 1;
				}
				else if ( $r2index == 0 && $r2decade == 0 ) {
					if ( $r1index == count($standard) - 1) {
						$r1decade++;
						$r1index = 0;
					}
					else {	
						$r1index++;
					}
				}
				else {
					$r2index--;
				}
			}
		}

		if ( $r1decade > 9 || $r2decade > 9 ) {
			if ( count($output) == 0 ) 
				echo("FAIL");

			$pairs = -1;
			break;
		}

		$r1 = $standard[$r1index] * pow(10,$r1decade);
		$r2 = $standard[$r2index] * pow(10,$r2decade);

		$rTprime = 1 / ( (1/$r1) + (1/$r2) );

		$result = checkPair($rTprime, $rT, $error);
		if ( $debug ) echo ("   Inside loop: r1=" . $r1 . " r2=" . $r2 . " rT=" . $rT . " error=" . $error . " result=" . $result . " loop: " . $loop . "<br />");

		$loop++;

	}	

	if ( $pairs >= 0 ) {  

		$output[$index] = array("R1" => $r1, "R2" => $r2, "RT" => round($rTprime, 2 - $input_decade) );
	 
		if ( $debug ) echo("R1=" . $r1 . " R2=" . $r2 . " <br />");

		$result = 1;
	}

	$pairs--;

	$index++;
}

if ( count($output) > 0 ) {
	$output = json_encode($output);
	echo($output);
}


function checkSTD($standard, $target, $decade ) {
	foreach ($standard as $key=>$resistor) {
		if ( $debug ) echo("Copmparing: " . $target . " with " . $resistor * $decade . "  decade: " . $decade . "<br>");
		if ( $target < ($resistor * $decade) ) {
			return $key*(-1);
		}
		else if ( $target == ($resistor * $decade) ) { 
			return $key;
		}
	}
	return count($standard) - 1;
}

function checkPair( $rTprime, $rT, $error ) {
	
	if ( $debug ) echo("rTprime=" . $rTprime . "<br />");
	if ( $rTprime < $rT - $error ) {
		return -1;
	}
	else if ( $rTprime > $rT + $error ) {
		return 1;
	}
	else {
		return 0;
	}
}


?>

